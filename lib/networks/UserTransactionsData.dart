import 'package:http/http.dart' as http;
import 'package:job1/models/UserTransactions.dart';
import 'dart:convert';

class UserTransactionsData{
  String url = "https://pfm.awinteck.com/api/client/transactions";
  String token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOjU3MjExLCJpYXQiOjE2MjQ0ODgyNzl9.xmvscC3rzTHrYlaCqCAt9bDrYYug6cRDZqDHTGOGb2o';
  // UserTransactionData(this.token);
  Future<List<UserTransactions>> getUserTransactions () async {
    print('LOADING DATA API');
    var path = Uri.parse(url);
    http.Response response = await http.get(path, headers: <String, String>{
      'token':token
    });
    var decodeResponse = jsonDecode(response.body);
    Iterable transactions = decodeResponse['response'];
    print(transactions);

    return transactions.map((single) => UserTransactions.fromJson(single)).toList();
  }
}