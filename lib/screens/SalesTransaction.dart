import 'package:flutter/material.dart';
import 'package:job1/components/TransactionLogo.dart';
import 'package:job1/components/button.dart';
import 'package:job1/screens/OrderHistory.dart';
import '../components/logo.dart';

class SaleTransaction extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;

    return MaterialApp(
      title: 'Sale Transaction',
      home: Scaffold(
        body: Center(
          child: ListView(
            children: [
              SizedBox(height: media.height*0.2,),
              logo(context),
              SizedBox(height: 10,),
              Center(child: Text('PeFUM',style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16), )),
              SizedBox(height: 10,),
              Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text('Complete',style: TextStyle(fontWeight: FontWeight.w700, fontSize: 30), ),
                      SizedBox(width: 10.0,),
                      Image.asset('./assets/images/okay.png', height: 33.0,)
                    ],
                  )
              ),
              SizedBox(height: 10,),
              Center(
                child: SizedBox(
                  width: media.width*0.8,  // Width of the card

                  //Transaction preview card {Cost, fuel type, oil company, location and transaction id}
                  child: Card(
                    shadowColor: Colors.black,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                    elevation: 6.0,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical:40.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('GHS 305.00', style: TextStyle(color: Color(0xffFFD01F),fontSize: 25,fontWeight: FontWeight.w900),),
                          Text('Super Petrol', style: TextStyle(color: Colors.grey.shade700,fontWeight: FontWeight.w900),),
                          SizedBox(height: 30.0,),

                          //LOGO with 2 Texts on the side
                          transactionLogo(context,logoPath: 'person.jpg', text1: 'Enoch Nketia', text2: '5502'),
                          SizedBox(height: 20,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Transaction ID', style: TextStyle(color: Colors.grey.shade700, fontWeight: FontWeight.w500)),
                              Text('#10382', style: TextStyle(color: Color(0xff8e8e8e),fontWeight: FontWeight.normal)),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              SizedBox(height: 30.0,),
              //  Button
              Center(
                child: Container(
                  width: media.width*0.8,  // width of button
                  child: button(context, text:'Done',
                      func: (){
                        Navigator.pop(context);
                      }),
                ),
              )

            ],
          ),
        ),
      ),
    );
  }
}
