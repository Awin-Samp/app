import 'package:flutter/material.dart';
import 'package:job1/components/TextInput.dart';
import 'package:job1/components/button.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Profile',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white,),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          title: Text('Profile'),
        ),
        body: Container(
          child: ListView(

            children: [
              SizedBox(height: 20),
              Center(
                child: CircleAvatar(
                  backgroundColor: Color(0xECECECCE),
                  child:  10==10 ?  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.camera_alt, size: 30, color: Colors.amber,),
                      Text('Upload Photo', style: TextStyle(fontSize: 13, color: Colors.amber),)
                    ],
                  ) : null,
                  radius: 60,
                  backgroundImage: 1==10 ? AssetImage('./assets/images/pefum.jpg'): null,
                ),
              ),
              SizedBox(height: 10,),
              Center(child: Text('Awin Samp', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),)),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal:20.0),
                child: Column(

                  children: [
                    SizedBox(height: 10,),
                    TextInput(title:'First Name', value: 'Awin',),
                    SizedBox(height: 10,),
                    TextInput(title:'Last Name', value: 'Samp',),
                    SizedBox(height: 10,),
                    TextInput(title:'Mobile Number', value: '+233547785025', enabledState: false, verified: 'Verified',),
                    SizedBox(height: 10,),
                    TextInput(title:'Email', value: 'awinsamp@yahoo.com', verified: 'Unverified',),
                    SizedBox(height: 10,),
                    TextInput(title:'PeFUM Code', value: '5501', enabledState: false,),
                    SizedBox(height: 10,),
                    TextInput(title:'Password', value: 'password', obscureState: true,),
                    SizedBox(height: 20,),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        FlatButton(
                        padding: EdgeInsets.symmetric(vertical: 2, horizontal: 30),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                          child: Text('CANCEL', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color:Colors.amber),),
                          onPressed: (){},
                        ),
                        RaisedButton(
                          padding: EdgeInsets.symmetric(vertical: 2, horizontal: 30),
                          color: Colors.black,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                          child: Text('SAVE', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color:Colors.white),),
                          onPressed: (){
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),

            ],
          )
        ),
      ),
    );
  }
}
