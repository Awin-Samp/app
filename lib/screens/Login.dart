import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:job1/screens/Dashboard.dart';
import 'package:job1/screens/Forgot_Password.dart';
import 'package:job1/screens/Preview_Transaction.dart';
import 'Register.dart';
import 'package:job1/components/TextInput.dart';
import 'package:job1/components/logo.dart';
import 'package:job1/components/button.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PeFUM APP - Log in',
     home: Scaffold(
       body: Container(
         color: Colors.white,
           child: new MyForm()),
     )
    );
  }
}
class MyForm extends StatefulWidget{
  _Myform createState()=> _Myform();
}

class _Myform extends State<MyForm>{

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    var orient = MediaQuery.of(context).orientation;
    // TODO: implement build
    return Center(
      child: Container(
        alignment: Alignment.center,
        height: media.height,
        width: orient==Orientation.landscape ? 400 : media.width*0.8,
          margin: EdgeInsets.fromLTRB(20, 23, 20, 10),
        child: Container(
          child: ListView(
            children: [
              SizedBox(height: media.height*0.2,),
              logo(context),
              SizedBox(height: 10,),
              Center(child: Text('PeFUM',style: TextStyle(fontWeight: FontWeight.w700, fontSize: 21), )),
              SizedBox(height: 40,),
              TextInput(title:'Mobile Number'),
              SizedBox(height: 20,),
              TextInput(title:'Password'),
              SizedBox(height: 20,),
              Container(
                alignment: Alignment.topRight,
                child: FlatButton(
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> ForgotPassword()));
                  },
                    child: Text('Forgot Password?', style: TextStyle(fontWeight: FontWeight.w300),))
              ),
              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: button(context,text: "Sign In", func: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> Dashboard()));
                }),
              ),
              SizedBox(height: 20,),
              Center(
                  child: FlatButton(
                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context)=> new Register()));},
                    child: RichText(
                      text: TextSpan(
                        text: "Don't have an account?",
                        style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18, color: Colors.black),
                        children: <TextSpan>[
                          TextSpan( text: ' Sign Up', style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18, color: Colors.amber),
                        ),
                        ]
                        ) ,
                      ),
                  ),
                  )
            ],
          )
        )
      ),
    );
  }
}
