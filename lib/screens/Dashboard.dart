import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:job1/components/ListItem.dart';
import 'package:job1/components/Logout.dart';
import 'package:job1/components/PurchaseDialog.dart';
import 'package:job1/components/TransactionLogo.dart';
import 'package:job1/utilities/TColors.dart';
import 'package:job1/models/UserTransactions.dart';
import 'package:job1/networks/UserTransactionsData.dart';
import 'package:job1/screens/OrderHistory.dart';
import 'package:job1/screens/Preview_Transaction.dart';
import '../components/secondaryButton.dart';
import 'Attendart_Dashboard.dart';
import 'Profile.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  Future<List<UserTransactions>> userTransactions;
  UserTransactionsData api = new UserTransactionsData();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    userTransactions = api.getUserTransactions();

  }

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    return MaterialApp(
      title: 'PeFUM app',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text('Dashboard'),
          actions: [
            GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> Attendant_Dashboard()));
                },
                child: Icon(Icons.notifications, color: Colors.white,)
            ),
            SizedBox(width: 20,)
          ],
        ),
        drawer: SafeArea(
          child: Drawer(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: myDrawer(),
            )
          ),
        ),
        body: Container(
          child: Column(
            children: [
              DashCard(),
              SizedBox(height: 30,),
              PurchaseCard(width: media.width*0.9),
              ApprovalsCard(width: media.width*0.9),
              SizedBox(height: 20,),
              Center(child: Text('Recent Transactions', style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700),)),

              Expanded(
                child: FutureBuilder(
                  future: userTransactions,
                  builder: (context, snapshot){
                    if(snapshot.hasData){
                      print('Testing hasData');
                      return ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index){
                          var item = snapshot.data[index];
                          var count = index +1;
                          return Column(
                            children: [
                              ListItem(context,logoPath: 'Shell.jpg', station: item.companyName, location: item.location,  price: 'GHC '+ item.amount, date: item.date.toString(),
                                  status: item.status,
                                  color: item.status=='completed'? Colors.green: 
                                  item.status=="pending"? Colors.blue:
                                  item.status=='low_balance'? Colors.purple: Colors.red,
                                  func: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
                                  } ),
                              count < snapshot.data.length?  Divider() :  Padding(
                                 padding: const EdgeInsets.symmetric(horizontal:30.0, vertical: 20),
                                child: GestureDetector(
                                    onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context)=> OrderHistory()));},
                                    child: Text('Show All', style: TextStyle(color: Colors.amber))
                                ),
                              )
                            ],
                          );
                        },
                      );
                    }else if(snapshot.hasError){
                      print('Testing hasError');
                      return Text('Error loading data');
                    }
                    print('Testing data is still loading hereeeee');
                    return Center(child: CircularProgressIndicator());
                  },
                )
              ),

            ],
          ),
        ),
      ),
    );
  }

  Widget DashCard(){
    return Container(
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20))
      ),
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          LeftDashcard(),
          Image.asset('./assets/images/cedi.png',width: 120,),

        ],
      ),
    );
  }

  Widget LeftDashcard(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Welcome Back', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),),
        Text('Awin Samp', style: TextStyle(color: Colors.white,fontWeight: FontWeight.w400, fontSize: 20)),
        SizedBox(height: 15.0,),
        Text('Wallet Balance', style: TextStyle(color: Colors.white,fontWeight: FontWeight.w300),),
        Text('GHS 5430.30',style: TextStyle(color: Color(0xffFFD01F),fontWeight: FontWeight.w600, fontSize: 22)),
        SizedBox(height: 10,),
        secondaryButton(context, text: 'Deposit', func: (){}),
      ],
    );
  }

  Widget PurchaseCard({double width}){
    return Container(
      width: width,
      child: GestureDetector(
        onTap: (){
          showDialog(context: context, builder: (context)=> PurchaseDialog(context));
          },
        child: Card(
          elevation: 10,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(width: 20,),
                  Image.asset('./assets/images/gas station.png', width: 25,),
                  SizedBox(width: 20,),
                  Text('Purchase Fuel', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),),
                ],
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.only(topRight: Radius.circular(20), bottomRight: Radius.circular(20)),
                ),
                width: 100,
                child: Column(
                  children: [
                    Text('Generate', textAlign: TextAlign.center, softWrap: true, style: TextStyle(color: Color(0xffFFD01F), fontSize: 12) ),
                    Text('QR Code', textAlign: TextAlign.center, softWrap: true, style: TextStyle(color: Color(0xffFFD01F), fontSize: 12) ),
                    SizedBox(height: 10,),
                    Image.asset('./assets/images/Scan icon.png', width: 40,),
                  ],
                )
              )

            ],
          ),
        ),
      ),
    );
  }

  Widget ApprovalsCard({double width}){
    return Container(
      width: width,
      child: GestureDetector(
        onTap: (){
          showDialog(context: context, builder: (context)=> CustomDialog(context, hasApprovalState: true));
          },
        child: Card(
          elevation: 10,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(width: 20,),
                  Image.asset('./assets/images/Approvals icon.png', width: 25,),
                  SizedBox(width: 20,),
                  Text('Pending Approvals', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),),
                ],
              ),
              Container(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.only(topRight: Radius.circular(20), bottomRight: Radius.circular(20)),
                  ),
                  width: 100,
                  child: Column(
                    children: [
                      Text('Approve', textAlign: TextAlign.center, softWrap: true, style: TextStyle(color: Color(0xffFFD01F), fontSize: 12) ),
                      Text('or Decline', textAlign: TextAlign.center, softWrap: true, style: TextStyle(color: Color(0xffFFD01F), fontSize: 12) ),
                      SizedBox(height: 10,),
                      Image.asset('./assets/images/PeFUM logo.png', width: 40,),
                    ],
                  )
              )

            ],
          ),
        ),
      ),
    );
  }

  Widget myDrawer(){
    return ListView(
      children: [
        SizedBox(height: 30,),
        ListTile(
          onTap: (){ Navigator.push(context, MaterialPageRoute(builder:(context)=> Profile() ));},
          leading: CircleAvatar(
            backgroundColor: Colors.black,
            child: Icon(Icons.person, size: 45, color: Colors.white,),
            radius: 35,
          ), title: Text('Awin Samp'),
          subtitle: Text('View profile', style: TextStyle(fontSize: 12)),

        ),
        SizedBox(height: 40,),
        Divider(),
        ListTile(
          onTap: (){ Navigator.push(context, MaterialPageRoute(builder:(context)=> OrderHistory() ));},
          leading: Icon(Icons.shopping_cart_sharp, color: Colors.black),
          title: Text('Order History'),
        ),
        Divider(),
        ListTile(
          leading: Icon(Icons.local_gas_station, color: Colors.black),
          title: Text('Find Fuel'),
        ),
        Divider(),
        ListTile(
          leading: Icon(Icons.sms_failed_sharp, color: Colors.black),
          title: Text('Support and Info'),
        ),
        Divider(),
        ListTile(
          leading: Icon(Icons.info, color: Colors.black),
          title: Text('About PeFUM'),
        ),
        Divider(),
        ListTile(
          leading: Icon(Icons.logout, color: Colors.black),
          title: Text('Logout'),
          onTap: (){
            showDialog(context: context, builder: (context)=> LogoutDialog(context));
          },
        ),

      ],
    );
  }

  Widget CustomDialog(BuildContext context,{ bool hasApprovalState = false}){
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Stack(
        children: [

          Container(
            padding: EdgeInsets.only(top: 40, bottom: 10),
            margin: EdgeInsets.only(top:40),
            width: 400,
            child: hasApprovalState ? haveApprovals(context) : noApprovals(context),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15 )
            ),
          ),
          Positioned(
            left: 20,
            right: 20,
            child: CircleAvatar(
              radius: 40, 
              backgroundColor: Colors.white,
              child: ClipRect(
                child: Image.asset('assets/images/Approvals icon.png',width: 50,),
              ),
            ),
          ),
        ],
      ),
    );

  }

  Widget haveApprovals(BuildContext context){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('Pending Approvals', style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500)),
        ListItem(context, logoPath: 'Shell.jpg', station: 'Shell oil', location: 'Haasto', price: 'GHS 200.00', date: '02:59 left',
            func: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
            } ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(width: 20,),
            Text('Fuel Type :',style: TextStyle(fontSize: 13,),),
            Text('Super Petrol', style: TextStyle( fontWeight: FontWeight.w800, color: Colors.green),),
          ],
        ),
        SizedBox(height: 20,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            FlatButton(
                onPressed: (){
                  Navigator.pop(context);
                  showDialog(context: context, builder: (context)=> CustomDialog(context, hasApprovalState: false));
                },
                color: Colors.black,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                child: Text('Decline', style: TextStyle(color: Colors.white),)
            ),
            FlatButton(
                onPressed: (){
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
                },
                color: Colors.amber,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                child: Text('Accept', style: TextStyle(color: Colors.white),)
            ),
          ],
        ),
        SizedBox(height: 20,),
        Text('Pending Approvals are only valid for 15 min', style: TextStyle(fontSize: 12, color: Colors.red.shade300),)
      ],
    );
  }

  Widget noApprovals(BuildContext context){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('Pending Approvals', style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500)),
        Text('No Approvals yet!'),
        SizedBox(height: 20,),
        FlatButton(
            onPressed: (){
              Navigator.pop(context);
            },
            color: Colors.black,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            child: Text('close', style: TextStyle(color: Colors.white),))
      ],
    );
  }


}

