import 'package:flutter/material.dart';
import 'package:job1/components/ListItem.dart';
import '../components/TransactionLogo.dart';
import 'SalesTransaction.dart';

class SalesHistory extends StatefulWidget {
  @override
  _SalesHistoryState createState() => _SalesHistoryState();
}

class _SalesHistoryState extends State<SalesHistory> {
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    var orient = MediaQuery.of(context).orientation;

    return MaterialApp(
      title: 'Order History',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white,),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          title: Text('Sales History'),
        ),
        body: Center(
          child: Container(
            width:  media.width*1,
            child: ListView(
              children: [
                ListItem(context,logoPath: 'person.jpg', station: 'Enoch Nketia', location: '5501', price: 'GHS 150.00', date: '4:30pm',
                    func: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
                    } ),
                Divider(),
                ListItem(context,logoPath: 'person.jpg', station: 'Awin Samp', location: '7402', price: 'GHS 200.00', date:'5:02pm',
                    func: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
                    } ),
                Divider(),
                ListItem(context,logoPath: 'person.jpg', station: 'Enoch Nketia', location: '5501', price: 'GHS 150.00', date: '4:30pm',
                    func: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
                    } ),
                Divider(),
                ListItem(context,logoPath: 'person.jpg', station: 'Awin Samp', location: '7402', price: 'GHS 200.00', date:'5:02pm',
                    func: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
                    } ),
                Divider(),
                ListItem(context,logoPath: 'person.jpg', station: 'Enoch Nketia', location: '5501', price: 'GHS 150.00', date: '4:30pm',
                    func: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
                    } ),
                Divider(),
                ListItem(context,logoPath: 'person.jpg', station: 'Awin Samp', location: '7402', price: 'GHS 200.00', date:'5:02pm',
                    func: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
                    } ),
                Divider(),

               ],
            ),
          ),
        ),
      ),
    );
  }

}
