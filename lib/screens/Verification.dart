import 'package:flutter/material.dart';
import 'package:job1/components/TInput.dart';
import 'package:job1/components/button.dart';
import 'package:job1/screens/Login.dart';
import 'package:job1/utilities/TColors.dart';
import 'package:job1/utilities/TStyles.dart';
import 'package:job1/utilities/TStrings.dart';

class Verification extends StatefulWidget {
  @override
  _VerificationState createState() => _VerificationState();
}

class _VerificationState extends State<Verification> {
  @override
  Widget build(BuildContext context) {
    final focus1 = FocusNode();
    final focus2 = FocusNode();
    final focus3 = FocusNode();
    var media = MediaQuery
        .of(context)
        .size;
    return MaterialApp(
        title: OTP_VER,
        home: Scaffold(
            body: Container(
                padding: EdgeInsets.symmetric(horizontal: 40.0),
                child: ListView(
                  children: [
                    SizedBox(height:20,),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: CircleAvatar(
                        backgroundColor: Colors.black,
                        radius: 30,
                        child: IconButton(
                            color: Colors.white,
                            onPressed: (){
                              Navigator.pop(context);
                            },
                            icon: Icon(Icons.arrow_back_sharp)),
                      ),
                    ),
                    SizedBox(height: media.height * 0.06,),
                    Text(OTP_VER, style: titleStyle,),
                    SizedBox(height: 30,),

                    //Creates ghanaflag with dropdown arrow and input box below
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          children: [
                            Image.asset('assets/images/Ghanaflag.png', width: 25,),
                            Icon(Icons.arrow_drop_down, color: grey,),
                            SizedBox(width: 10,),
                            Flexible(child: TInput(context, editable: false,
                                value: '+233 547785025',
                                title: MOBILE_NUMBER,
                                type: TextInputType.phone)),
                          ],
                        ),
                        Divider(
                          height: 1, thickness: 1, color: Color(0xFFD3D3D3),)
                      ],
                    ),
                    SizedBox(height: 30,),

                    Text(ENTER_OTP, style: TextStyle(color: Colors.grey,
                        fontSize: 17,
                        fontWeight: FontWeight.w300)),
                    SizedBox(height: 10,),
                    Row(
                      children: [
                        Text('+233 547785025', style: TextStyle(
                          color: Colors.black, fontSize: 17,)),
                        FlatButton(onPressed: () {
                          // Navigator.push(context, MaterialPageRoute(builder:(context)=> UpdatePhoneNumber()));
                        },
                            child: Text(EDIT, style: TextStyle(
                                color: Colors.blue,
                                fontSize: 17,
                                fontWeight: FontWeight.normal),)),
                      ],
                    ),
                    SizedBox(height: 20,),
                    // Four input forms for OTP verification code
                    Form(
                      child: Row(
                        children: [
                          Flexible(child: TextFormField(
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            maxLengthEnforced: true,
                            maxLines: 1,
                            maxLength: 1,
                            enableSuggestions: false,
                            decoration: InputDecoration(
                                hintText: '', counterText: '',
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Color(0xFFD3D3D3))
                                )),
                            onFieldSubmitted: (v) =>
                                FocusScope.of(context)
                                    .requestFocus(focus1),
                          )),
                          SizedBox(width: 5,),
                          Flexible(child: TextFormField(
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            maxLengthEnforced: true,
                            maxLines: 1,
                            focusNode: focus1,
                            maxLength: 1,
                            enableSuggestions: false,
                            decoration: InputDecoration(
                              hintText: '', counterText: '',
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xFFD3D3D3))
                              ),
                            ),
                            onFieldSubmitted: (v) =>
                                FocusScope.of(context)
                                    .requestFocus(focus2),
                          )),
                          SizedBox(width: 5,),
                          Flexible(child: TextFormField(
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            maxLengthEnforced: true,
                            maxLines: 1,
                            focusNode: focus2,
                            maxLength: 1,
                            enableSuggestions: false,
                            decoration: InputDecoration(
                              hintText: '', counterText: '',
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xFFD3D3D3))
                              ),),
                            onFieldSubmitted: (v) =>
                                FocusScope.of(context)
                                    .requestFocus(focus3),
                          )),
                          SizedBox(width: 5,),
                          Flexible(child: TextFormField(
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            maxLengthEnforced: true,
                            maxLength: 1,
                            focusNode: focus3,
                            enableSuggestions: false,
                            decoration: InputDecoration(
                              hintText: '', counterText: '',
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xFFD3D3D3))
                              ),),
                          )),
                        ],
                      ),
                    ),
                    SizedBox(height: 30,),
                   button(context, text: SUBMIT, func: (){
                     Navigator.push(context, MaterialPageRoute(builder: (context)=> Login()));
                   },),
                    SizedBox(height: 20,),
                    Center(child: Text('$RESEND_CODE 02:59'))
                  ],
                )
            )
        )
    );
  }
}
 