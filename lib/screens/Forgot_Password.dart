import 'package:flutter/material.dart';
import 'package:job1/components/TInput.dart';
import 'package:job1/components/button.dart';
import 'package:job1/screens/Reset_Password.dart';
import 'package:job1/utilities/TColors.dart';
import 'package:job1/utilities/TStrings.dart';
import 'package:job1/utilities/TStyles.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    final focus1 = FocusNode();
    final focus2 = FocusNode();
    final focus3 = FocusNode();
    var media = MediaQuery
        .of(context)
        .size;
    return MaterialApp(
        title: OTP_VER,
        home: Scaffold(
            body: Container(
                padding: EdgeInsets.symmetric(horizontal: 40.0),
                child: ListView(
                  children: [
                    SizedBox(height:20,),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: CircleAvatar(
                        backgroundColor: Colors.black,
                        radius: 30,
                        child: IconButton(
                          color: Colors.white,
                            onPressed: (){
                            Navigator.pop(context);
                            },
                            icon: Icon(Icons.arrow_back_sharp)),
                      ),
                    ),
                    SizedBox(height: media.height * 0.06,),
                    Text('Forgot Password', style: titleStyle,),
                    SizedBox(height: 30,),

                    Row(
                      children: [
                        Icon(Icons.phone, color: grey,),
                        SizedBox(width: 10,),
                        Flexible(
                          child: TextFormField(
                            keyboardType: TextInputType.phone,
                            enabled: false,
                            initialValue: '+233 547785025',
                            decoration: InputDecoration(
                              labelText: 'Mobile Number',
                              labelStyle: TextStyle(color: grey),
                              contentPadding: EdgeInsets.only(top: 20.0),
                              border: UnderlineInputBorder(borderSide: BorderSide.none,),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Divider(),

                    SizedBox(height: 20,),

                    Text('Enter the 4-digit code sent to you via SMS', style: TextStyle(color: Colors.grey,
                        fontSize: 15,
                        fontWeight: FontWeight.w300)),
                    SizedBox(height: 10,),

                    // Four input forms for OTP verification code
                    Form(
                      child: Row(
                        children: [
                          Flexible(child: TextFormField(
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            maxLengthEnforced: true,
                            maxLines: 1,
                            maxLength: 1,
                            enableSuggestions: false,
                            decoration: InputDecoration(
                                hintText: '', counterText: '',
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Color(0xFFD3D3D3))
                                )),
                            onFieldSubmitted: (v) =>
                                FocusScope.of(context)
                                    .requestFocus(focus1),
                          )),
                          SizedBox(width: 5,),
                          Flexible(child: TextFormField(
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            maxLengthEnforced: true,
                            maxLines: 1,
                            focusNode: focus1,
                            maxLength: 1,
                            enableSuggestions: false,
                            decoration: InputDecoration(
                              hintText: '', counterText: '',
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xFFD3D3D3))
                              ),
                            ),
                            onFieldSubmitted: (v) =>
                                FocusScope.of(context)
                                    .requestFocus(focus2),
                          )),
                          SizedBox(width: 5,),
                          Flexible(child: TextFormField(
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            maxLengthEnforced: true,
                            maxLines: 1,
                            focusNode: focus2,
                            maxLength: 1,
                            enableSuggestions: false,
                            decoration: InputDecoration(
                              hintText: '', counterText: '',
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xFFD3D3D3))
                              ),),
                            onFieldSubmitted: (v) =>
                                FocusScope.of(context)
                                    .requestFocus(focus3),
                          )),
                          SizedBox(width: 5,),
                          Flexible(child: TextFormField(
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            maxLengthEnforced: true,
                            maxLength: 1,
                            focusNode: focus3,
                            enableSuggestions: false,
                            decoration: InputDecoration(
                              hintText: '', counterText: '',
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Color(0xFFD3D3D3))
                              ),),
                          )),
                        ],
                      ),
                    ),
                    SizedBox(height: 30,),
                    button(context, text: SUBMIT, func: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> ResetPassword()));
                    },),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('$RESEND_CODE',),
                        Text(' 02:59', style: TextStyle(color: Colors.amber),),
                      ],
                    )
                  ],
                )
            )
        )
    );
  }
}
