import 'package:flutter/material.dart';
import 'package:job1/components/TextInput.dart';
import 'package:job1/components/button.dart';
import 'package:job1/screens/Login.dart';
import 'package:job1/utilities/TStrings.dart';
import 'package:job1/utilities/TStyles.dart';

class ResetPassword extends StatefulWidget {
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery
        .of(context)
        .size;
    return MaterialApp(
      title: 'Reset Password',
      home: Scaffold(
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 40.0),
          child: ListView(
            children: [
              SizedBox(height:20,),
              Align(
                alignment: Alignment.centerLeft,
                child: CircleAvatar(
                  backgroundColor: Colors.black,
                  radius: 30,
                  child: IconButton(
                      color: Colors.white,
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.arrow_back_sharp)),
                ),
              ),
              SizedBox(height: media.height * 0.06,),
              Text('Reset Password', style: titleStyle,),
              SizedBox(height: 30,),

              TextInput(title: 'Password',obscureState: true, ErrorText: 'Enter your Password',),
              SizedBox(height: 30,),
              TextInput(title: 'Repeat Password',obscureState: true, ErrorText: 'Enter your Password',),
              SizedBox(height: 30,),
              button(context, text: "Reset Password", func: (){
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context)=> Login()));
              },),
            ],
          ),
        ),
      ),
    );
  }
}
