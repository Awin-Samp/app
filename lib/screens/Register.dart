import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:job1/components/TextInput.dart';
import 'package:job1/components/logo.dart';
import 'package:job1/components/button.dart';
import 'package:job1/screens/Login.dart';
import 'package:job1/screens/Verification.dart';

class Register extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'PeFUM APP - Register',
        home: Scaffold(
          body: Container(
              color: Colors.white,
              child: new MyForm()),
        )
    );
  }
}
class MyForm extends StatefulWidget{
  _Myform createState()=> _Myform();
}

class _Myform extends State<MyForm>{

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    var orient = MediaQuery.of(context).orientation;
    // TODO: implement build
    return Center(
      child: Container(
          alignment: Alignment.center,
          height: media.height,
          width: orient==Orientation.landscape ? 400 : media.width*0.8,
          margin: EdgeInsets.fromLTRB(20, 23, 20, 10),
          child: Container(
              child: ListView(
                children: [
                  SizedBox(height: media.height*0.2,),
                  logo(context),
                  SizedBox(height: 10,),
                  Center(child: Text('PeFUM',style: TextStyle(fontWeight: FontWeight.w700, fontSize: 21), )),
                  SizedBox(height: 10,),
                  TextInput(title:'First Name'),
                  SizedBox(height: 10,),
                  TextInput(title:'Last Name'),
                  SizedBox(height: 10,),
                  TextInput(title:'Email'),
                  SizedBox(height: 10,),
                  TextInput(title:'Mobile Number'),
                  SizedBox(height: 10,),
                  TextInput(title:'Password'),
                  SizedBox(height: 20,),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: button(context, text:'Create Account', func: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> Verification()));
                    }),
                  ),
                  SizedBox(height: 20,),
                  Center(
                    child: FlatButton(
                      onPressed: (){Navigator.pop(context); Navigator.push(context, MaterialPageRoute(builder: (context)=> Login()));},
                      child: RichText(
                        text: TextSpan(
                            text: "Already have an account?",
                            style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18, color: Colors.black),
                            children: <TextSpan>[
                              TextSpan( text: ' Sign In', style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18, color: Colors.amber),
                              ),
                            ]
                        ) ,
                      ),
                    ),
                  )
                ],
              )
          )
      ),
    );
  }
}
