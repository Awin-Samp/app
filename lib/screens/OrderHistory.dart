import 'package:flutter/material.dart';
import 'package:job1/models/UserTransactions.dart';
import 'package:job1/networks/UserTransactionsData.dart';
import '../components/TransactionLogo.dart';
import 'package:job1/components/ListItem.dart';

import 'SalesTransaction.dart';

class OrderHistory extends StatefulWidget {
  @override
  _OrderHistoryState createState() => _OrderHistoryState();
}

class _OrderHistoryState extends State<OrderHistory> {

  Future<List<UserTransactions>> userTransactions;
  UserTransactionsData api = new UserTransactionsData();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    userTransactions = api.getUserTransactions();

  }

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    var orient = MediaQuery.of(context).orientation;

    return MaterialApp(
      title: 'Order History',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white,),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          title: Text('Order History'),
        ),
        body: Center(
          child: Container(
            width:  media.width*0.9,
            child: FutureBuilder(
              future: userTransactions,
              builder: (context, snapshot){
                if(snapshot.hasData){
                  print('Testing hasData');
                  return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index){
                      var item = snapshot.data[index];
                      return Column(
                        children: [
                          ListItem(context,logoPath: 'Shell.jpg', station: item.companyName, location: item.location,  price: 'GHC '+ item.amount, date: item.date.toString(),
                              status: item.status,
                              color: item.status=='completed'? Colors.green:
                              item.status=="pending"? Colors.blue:
                              item.status=='low-balance'? Colors.purple: Colors.red,
                              func: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
                              } ),
                          Divider(),
                        ],
                      );
                    },
                  );
                }else if(snapshot.hasError){
                  print('Testing hasError');
                  return Text('Error loading data');
                }
                print('Testing data is still loading hereeeee');
                return Center(child: CircularProgressIndicator());
              },
            )
          ),
        ),
      ),
    );
  }
}
