import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:job1/components/AttendantDrawer.dart';
import 'package:job1/components/CorporateCodeDialog.dart';
import 'package:job1/components/ListItem.dart';
import 'package:job1/components/PCodeDialog.dart';
import 'package:job1/components/CorporateCodeDialog.dart';
import 'package:job1/components/ScannedCodeDialog.dart';

import 'SalesHistory.dart';
import 'SalesTransaction.dart';

class Attendant_Dashboard extends StatefulWidget {
  @override
  _Attendant_DashboardState createState() => _Attendant_DashboardState();
}

class _Attendant_DashboardState extends State<Attendant_Dashboard> {
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    return MaterialApp(
      title: 'Home',
      home: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text('Dashboard', style: TextStyle(color: Colors.black),),
          actions: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Text('ID: 605541',style: TextStyle(color: Colors.black)),

            )
          ],
        ),
        drawer:  SafeArea(
          child: Drawer(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: myDrawer(context),
              )
          ),
        ),
        body: Container(
          color: Colors.white,
          child: ListView(
            children: [
              SizedBox(height: 10,),
              ProfileHead(),
              FlatButton(
                onPressed: (){

                  Navigator.push(context, MaterialPageRoute(builder: (context)=> SalesHistory()));
                },
              child: SalesCard(),
          ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical : 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CodeCard(text: ['PeFUM', 'Code'], func: (){
                      showDialog(context: context, builder: (context)=> PCodeDialog(context, width: media.width* 0.65));
                    }),
                    CodeCard(text: ['Corporate', 'Code'],func: (){
                      showDialog(context: context, builder: (context)=> CorporateCodeDialog(context, width: media.width*0.65));
                    }),
                  ],
                ),
              ),

              // QRCODE scanner button here
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical : 20),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Image.asset('./assets/images/scanner.png', fit: BoxFit.contain)
                  ),
                    FlatButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                        padding: EdgeInsets.symmetric(horizontal: 178, vertical: 98),
                        onPressed: (){
                          showDialog(context: context, builder: (context)=> ScannedCodeDialog(context, width: media.width*0.65));
                        },
                        color: Colors.transparent,
                        splashColor: Colors.amber,
                    )
          ]
                ),
              ),
              SizedBox(height: 10),
              Center(child: Text('Recent Sales', style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700),)),

              Column(
                children: [
                  ListItem(context,logoPath: 'person.jpg', station: 'Enoch Nketia', location: '5501', price: 'GHS 150.00', date: '4:30pm',
                      func: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
                      } ),
                  Divider(),
                  ListItem(context,logoPath: 'person.jpg', station: 'Awin Samp', location: '7402', price: 'GHS 200.00', date:'5:02pm',
                      func: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> SaleTransaction()));
                      } ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal:30.0, vertical: 20),
                    child: GestureDetector(
                        onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context)=> SalesHistory()));},
                        child: Text('Show All', style: TextStyle(color: Colors.amber))
                    ),
                  )

                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget ProfileHead(){
    return Container(
      margin: EdgeInsets.all(20),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Image.asset('./assets/images/person.jpg', height: 60,),
          ),
          SizedBox(width: 10,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Kwame Frimpong', style: TextStyle(color: Colors.black,fontWeight: FontWeight.w400, fontSize: 20)),
              RichText(
                text: TextSpan(
                  text: "Total Station",
                  style: TextStyle(color: Colors.black, fontWeight: FontWeight.w300),
                  children: [
                    TextSpan(
                      text: ' '
                    ),
                    TextSpan(
                        text: 'MADINA',
                        style: TextStyle(color: Colors.amberAccent, fontWeight: FontWeight.bold)
                    )
                  ]
                ),
              ),

            ],
          )
        ],
      ),
    );
  }

  Widget SalesCard(){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(20)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Total Sales', style: TextStyle(color: Colors.white,fontWeight: FontWeight.w400, fontSize: 12)),
              RichText(
                text: TextSpan(
                    text: "GHS",
                    style: TextStyle(color: Colors.amberAccent, fontWeight: FontWeight.bold, fontSize: 25),
                    children: [
                      TextSpan(text: ' '),
                      TextSpan(
                          text: '9,210.00',
                          style: TextStyle(color: Colors.amberAccent, fontWeight: FontWeight.bold)
                      )
                    ]
                ),
              ),
              RichText(
                text: TextSpan(
                    text: "Vehicles Serviced",
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
                    children: [
                      TextSpan(
                          text: ': '
                      ),
                      TextSpan(
                          text: '23',
                          style: TextStyle(color: Colors.amberAccent, fontWeight: FontWeight.bold)
                      )
                    ]
                ),
              ),
            ],
          ),

          Icon(Icons.arrow_forward,size: 30, color: Colors.white,)
        ],
      ),
    );
  }

  Widget CodeCard({List<String> text, var func}){
    return RaisedButton(
      onPressed: func,
      color: Colors.grey.shade100,
      elevation: 0,
      splashColor: Colors.amberAccent,
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10)),
      child: Container(
        width: 120,
        padding: EdgeInsets.symmetric(vertical:30, horizontal: 10),
        child: Column(
          children: [
            Text(text[0], style: TextStyle(fontSize: 12),),
            Text(text[1], style: TextStyle(fontSize: 12),),
          ],
        ),
      ),
    );
  }
}
