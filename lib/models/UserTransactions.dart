

class UserTransactions{
  var transactionId;
  var pefumCode;
  var couponCode;
  var attendantCode;
  var companyName;
  var location;
  var logo;
  var amount;
  var fuelType;
  var status;
  var date;
  var time;

  UserTransactions(
      this.transactionId,
      this.pefumCode,
      this.couponCode,
      this.attendantCode,
      this.companyName,
      this.location,
      this.logo,
      this.amount,
      this.fuelType,
      this.status,
      this.date,
      this.time
      );

  factory UserTransactions.fromJson(Map<String, dynamic> json)=> UserTransactions(
        json['transaction_id'],
        json['pefum_code'],
        json['coupon_code'],
        json['attendant_code'],
        json['company_name'],
        json['location'],
        json['logo'],
        json['amount'],
        json['fuel_type'],
        json['status'],
        json['my_date'],
        json['my_time']
    );

}