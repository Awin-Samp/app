
class AttendantTransactions{
  var transactionId;
  var pefumCode;
  var attendantCode;
  var amount;
  var fuelType;
  var status;
  var firstName;
  var lastName;
  var profileImage;
  var date;
  var time;

  AttendantTransactions(
      this.transactionId,
      this.pefumCode,
      this.attendantCode,
      this.amount,
      this.fuelType,
      this.status,
      this.firstName,
      this.lastName,
      this.profileImage,
      this.date,
      this.time
      );

  factory AttendantTransactions.fromJson(Map<String, dynamic> json)=> AttendantTransactions(
      json['transaction_id'],
      json['pefum_code'],
      json['attendant_code'],
      json['amount'],
      json['fuel_type'],
      json['status'],
      json['first_name'],
      json['last_name'],
      json['profile_image'],
      json['date'],
      json['time']
  );
}