library t_strings;

const String TROWIN = 'Trowin';
const String SIGN_IN = 'SIGN IN';
const String SIGN_UP = 'SIGN UP';
const String REGISTER = 'REGISTER';
const String ALREADY = 'Already have an account';
const String FORGOT = 'Forgot Password?';

const String FIRST_NAME = 'First Name';
const String LAST_NAME = 'Last Name';
const String EMAIL = 'Email';
const String MOBILE_PHONE = 'Mobile Phone';
const String MOBILE_NUMBER = 'Mobile Number';
const String PASSWORD = 'Password';
const String CREATE_PROFILE = 'Create Profile';

const List TERMS_OF_CONDITIONS = ['By continuing, I confirm that I have read & agree to the ', 'Terms & conditions', ' and ', 'Privacy policy'];

const String OTP_VER = 'OTP Verification';
const String ENTER_OTP = 'Enter the 4-digit code sent to you at';
const String SUBMIT = 'SUBMIT';
const String EDIT = 'Edit';
const String RESEND_CODE = 'Resend code in';


//Profile page
const String PROFILE = 'Profile';
const String LOGOUT = 'LOG OUT';
const String SAVE = 'SAVE';
const String VERIFIED = 'Verified';
const String UNVERIFIED = 'Unverified';


const String ENTER_PASSWORD = 'Enter a new password';
const String SECURE_PASSWORDS_ARE = 'Secure passwords are at least 8 characters long and include numbers and symbols.';
const String UPDATE_PASSWORD = 'Update Password';
const String ENTER_EMAIL_ADDRESS = 'Enter your Email address';
const String ENTER_EMAIL_PHRASE = 'Enter in your email address to connect it to your Trowin account';
const String MOBILE_PHONE_PHRASE = 'Enter in your new Mobile number to connect it to your Trowin account';
const String UPDATE_NUMBER = 'Update Number';