library tcolors;
import 'package:flutter/material.dart';

const Color primaryColor = Colors.purple;
final Color secondaryColor = Colors.grey[350];
const Color grey = Colors.grey;
const Color white = Colors.white;
const Color black = Colors.black;
const Color Failed = Colors.red;
const Color Completed = Colors.green;
const Color Pending = Colors.yellow;
const Color lowBalance = Colors.blue;