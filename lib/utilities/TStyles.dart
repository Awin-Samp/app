library TStyles;

import 'package:flutter/material.dart';


const TextStyle titleStyle = TextStyle( fontSize: 24, fontWeight: FontWeight.w700);
const TextStyle smallStyle =  TextStyle(color:Colors.grey, fontSize: 13);