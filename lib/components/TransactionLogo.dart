import 'package:flutter/material.dart';

Widget transactionLogo(BuildContext context, {String logoPath='PeFUM logo.png', String text1='Oil Station', String text2='East Legon'}){
  return  Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
          elevation: 6,
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
                child: Image.asset('./assets/images/$logoPath', height: 45.0,)),
          )),
      SizedBox(width: 10.0,),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(text1, style: TextStyle(color: Colors.grey.shade700, fontSize: 18,fontWeight: FontWeight.w500)),
          Text(text2, style: TextStyle(color: Color(0xff8e8e8e),fontWeight: FontWeight.normal)),
        ],
      )
    ],
  );
}