import 'package:flutter/material.dart';


Widget PurchaseDialog(BuildContext context){
  return Dialog(
    backgroundColor: Colors.transparent,
    child: Stack(
      children: [

        Container(
          padding: EdgeInsets.only(top: 40, bottom: 10),
          margin: EdgeInsets.only(top:40),
          width: 400,
          child:Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Purchase Fuel', style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500)),

              SizedBox(height: 10,),
              Text('Allow Fuel attendant to scan',textAlign: TextAlign.center, style: TextStyle(fontSize: 12, color: Colors.amber),),
              ClipRect(
                child: Image.asset('assets/images/qr-code.png', height: 200,),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(width: 20,),
                  Text('PeFUM Code : ',style: TextStyle(fontSize: 13,),),
                  Text('5504', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800, color: Colors.green),),
                ],
              ),
              SizedBox(height: 20,),
              FlatButton(
                onPressed: (){
                  Navigator.pop(context);
                },
                color: Colors.black,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                child: Icon(Icons.close, color: Colors.white,),
              ),
            ],
          ),

          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15 )
          ),
        ),
        Positioned(
          left: 20,
          right: 20,
          child: CircleAvatar(
            radius: 40,
            backgroundColor: Colors.white,
            child: ClipRect(
              child: Image.asset('assets/images/PeFUM logo.png',width: 50,),
            ),
          ),
        ),
      ],
    ),
  );

}