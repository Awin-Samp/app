import 'package:flutter/material.dart';

Widget button(BuildContext context,{String text, var func}){
  return  RaisedButton(
      padding: EdgeInsets.symmetric(vertical: 13),
      color: Colors.black,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      child: Text(text, style: TextStyle(fontSize: 21, fontWeight: FontWeight.w400, color:Colors.white),),
      onPressed: func,
  );
}