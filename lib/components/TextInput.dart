import 'package:flutter/material.dart';

class TextInput extends StatelessWidget {
  final String title;
  final String ErrorText;
  final String value;
  final String verified;
  final bool obscureState;
  final bool enabledState;
  TextInput({this.title, this.verified='', this.value="",this.enabledState=true, this.obscureState=false, this.ErrorText="Enter your First Name"});

  @override
  Widget build(BuildContext context) {

    return TextFormField(
      obscureText: obscureState,
      enabled: enabledState,
      decoration: InputDecoration(
        labelText: title,
        suffixText: verified,
        suffixStyle: TextStyle(color: verified== 'Verified'? Colors.amber: Colors.red, fontSize: 12),
        labelStyle: TextStyle(fontWeight: FontWeight.w300),
        fillColor: Colors.grey,
      ),
        validator: (value)=> value.isEmpty? ErrorText : null,
      initialValue: value,
      style: TextStyle(color: !enabledState ? Colors.grey : Colors.black),


    );
  }
}
