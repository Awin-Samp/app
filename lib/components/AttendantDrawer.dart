import 'package:flutter/material.dart';
import 'package:job1/components/Logout.dart';
import 'package:job1/screens/SalesHistory.dart';

Widget myDrawer(BuildContext context){
  return ListView(
    children: [
      SizedBox(height: 30,),
      ListTile(
        onTap: (){ },
        leading: CircleAvatar(
          backgroundColor: Colors.black,
          child: Icon(Icons.person, size: 45, color: Colors.white,),
          radius: 35,
        ), title: Text('Kwame Frimpong'),
        subtitle: Text('ID: 605541', style: TextStyle(fontSize: 12)),
      ),
      SizedBox(height: 40,),
      Divider(),
      ListTile(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(builder: (context)=> SalesHistory()));
        },
        leading: Icon(Icons.shopping_cart_sharp, color: Colors.black),
        title: Text('Sales History'),
      ),
      Divider(),
      ListTile(
        leading: Icon(Icons.sms_failed_sharp, color: Colors.black),
        title: Text('Support and Info'),
      ),
      Divider(),
      ListTile(
        leading: Icon(Icons.info, color: Colors.black),
        title: Text('About PeFUM'),
      ),
      Divider(),
      ListTile(
        leading: Icon(Icons.logout, color: Colors.black),
        title: Text('Logout'),
        onTap: (){
          showDialog(context: context, builder: (context)=> LogoutDialog(context));
        },
      ),

    ],
  );
}
