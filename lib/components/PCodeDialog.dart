import 'package:flutter/material.dart';



Widget PCodeDialog(BuildContext context, {double width, bool clicked=true}){
  return Dialog(
    backgroundColor: Colors.transparent,
    child: Stack(
      children: [

        Container(
          padding: EdgeInsets.only(top: 40, bottom: 10),
          margin: EdgeInsets.only(top:40),
          width: 400,
          child:SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Send Request', style: TextStyle(fontSize: 17, fontWeight: FontWeight.w800, color: Colors.black)),

                SizedBox(height: 10,),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [

                  //  PETROL BUTTON
                  RaisedButton(
                  onPressed: (){},
                  color: clicked? Colors.amberAccent: Colors.grey.shade100,
                  elevation: 0,
                  splashColor: Colors.amberAccent,
                  shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10)),
                  child: Container(
                    width: 50,
                    padding: EdgeInsets.symmetric(vertical:30, horizontal: 1),
                    child: Column(
                      children: [
                        Text('Petrol', style: TextStyle(fontSize: 12),),
                      ],
                    ),
                  ),
                ),

                    //DIESEL BUTTON
                    RaisedButton(
                      onPressed: (){},
                      color: Colors.grey.shade100,
                      elevation: 0,
                      splashColor: Colors.amberAccent,
                      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10)),
                      child: Container(
                        width: 50,
                        padding: EdgeInsets.symmetric(vertical:30, horizontal: 1),
                        child: Column(
                          children: [
                            Text('Diesel', style: TextStyle(fontSize: 12),),
                          ],
                        ),
                      ),
                    ),

                    //LPGas BUTTON
                    RaisedButton(
                      onPressed: (){},
                      color: Colors.grey.shade100,
                      elevation: 0,
                      splashColor: Colors.amberAccent,
                      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10)),
                      child: Container(
                        width: 50,
                        padding: EdgeInsets.symmetric(vertical:30, horizontal: 1),
                        child: Column(
                          children: [
                            Text('LPGas', style: TextStyle(fontSize: 12),),
                          ],
                        ),
                      ),
                    ),

                  ],
                ),

                SizedBox(height: 10,),

                //ELECTRIC BUTTON
                RaisedButton(
                  onPressed: (){},
                  color: Colors.grey.shade100,
                  elevation: 0,
                  splashColor: Colors.amberAccent,
                  shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10)),
                  child: Container(
                    width: width,
                    padding: EdgeInsets.symmetric(vertical:30, horizontal: 1),
                    child: Column(
                      children: [
                        Text('ELECTRIC', style: TextStyle(fontSize: 12),),
                      ],
                    ),
                  ),
                ),

                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal:20.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide()
                      ),
                      labelText: 'Amount(GHS)',
                      labelStyle: TextStyle(fontSize: 13),
                      suffix: Text('min. 10GHS', style: TextStyle(fontSize: 12),),
                    ),
                  ),
                ),

                SizedBox(height: 10),

                //PEFUM CODE INPUTBOX
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal:20.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    maxLength: 6,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide()
                      ),
                      labelText: 'Enter PeFUM Code',
                      labelStyle: TextStyle(fontSize: 13)
                    ),
                  ),
                ),

                SizedBox(height: 20,),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    FlatButton(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      color: Colors.black,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                      child: Icon(Icons.close, color: Colors.white,),
                    ),
                    FlatButton(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      color: Colors.amber,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                      child: Icon(Icons.check, color: Colors.white,),
                    ),
                  ],
                ),
              ],
            ),
          ),

          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15 )
          ),
        ),
        Positioned(
          left: 20,
          right: 20,
          child: CircleAvatar(
            radius: 40,
            backgroundColor: Colors.white,
            child: ClipRect(
              child: Image.asset('assets/images/PeFUM logo.png',width: 50,),
            ),
          ),
        ),
      ],
    ),
  );

}