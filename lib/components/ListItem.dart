import 'package:flutter/material.dart';

import 'TransactionLogo.dart';

Widget ListItem(BuildContext context, {String logoPath,station,location,price='GHS 400.00',date='Mar 15', status='', Color color, var func})=> FlatButton(
  onPressed: func,
  splashColor: Colors.amberAccent,
  padding: EdgeInsets.all(0),
  child: Padding(
    padding: const EdgeInsets.symmetric(vertical:8.0, horizontal: 20),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        transactionLogo(context, logoPath: logoPath, text1: station, text2: location),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(price,style: TextStyle(color: Color(0xffFFD01F),fontWeight: FontWeight.normal)),
            Text(date, style: TextStyle(color: Color(0xff8e8e8e),fontWeight: FontWeight.normal)),
            Text(status, style: TextStyle(color: color,fontWeight: FontWeight.normal))
          ],
        )
      ],
    ),
  ),
);
