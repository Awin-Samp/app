import 'package:flutter/material.dart';

Widget secondaryButton(BuildContext context,{String text, var func}){
  return  RaisedButton(
    padding: EdgeInsets.symmetric(vertical: 2, horizontal: 30),
    color: Color(0xffffd01f),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
    child: Text(text, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color:Colors.black),),
    onPressed: func,
  );
}