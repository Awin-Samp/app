import 'package:flutter/material.dart';



Widget LogoutDialog(BuildContext context){
  return Dialog(
    backgroundColor: Colors.transparent,
    child: Stack(
      children: [

        Container(
          padding: EdgeInsets.only(top: 40, bottom: 10),
          margin: EdgeInsets.only(top:40),
          width: 400,
          child:Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('LOG OUT', style: TextStyle(fontSize: 17, fontWeight: FontWeight.w800, color: Colors.redAccent)),

              SizedBox(height: 10,),
              Text('Are you sure you want to Log out?'),
              SizedBox(height: 20,),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    color: Colors.black,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                    child: Icon(Icons.close, color: Colors.white,),
                  ),
                  FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    color: Colors.amber,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                    child: Icon(Icons.check, color: Colors.white,),
                  ),
                ],
              ),
            ],
          ),

          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15 )
          ),
        ),
        Positioned(
          left: 20,
          right: 20,
          child: CircleAvatar(
            radius: 40,
            backgroundColor: Colors.white,
            child: ClipRect(
              child: Image.asset('assets/images/PeFUM logo.png',width: 50,),
            ),
          ),
        ),
      ],
    ),
  );

}