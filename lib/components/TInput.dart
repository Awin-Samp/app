import 'package:flutter/material.dart';
import 'package:job1/utilities/TColors.dart';

Widget TInput(
  BuildContext context, {
  String title,
  String hint = '',
  String value = '',
  Color hintColor = grey,
  bool editable = true,
  TextInputType type,
  bool hide = false,
}) {
  return TextFormField(
    keyboardType: type,
    obscureText: hide,
    enabled: editable,
    initialValue: value,
    decoration: InputDecoration(
      hintText: hint,
      labelText: title,
      labelStyle: TextStyle(color: grey),
      hintStyle: TextStyle(color: hintColor),
      // suffixIcon: !showPasswordIcon ?
      //     null
      //     :
      //     hide== true?
      //       IconButton(icon: Icon(Icons.remove_red_eye_outlined), onPressed: (){ func(value){hide = value;}},)
      //         :
      //       IconButton(icon: Icon(Icons.close), onPressed: (){ func(value){ hide = value; }})
      // ,
      contentPadding: EdgeInsets.symmetric(vertical: 20.0),
      enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color(0xFFD3D3D3))),
    ),
  );
}
