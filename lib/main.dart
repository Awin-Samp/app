import 'package:flutter/material.dart';
import 'screens/Login.dart';
import 'components/logo.dart';

void main()=> runApp(App());

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'Job MaterialApp',
      home: Scaffold(
        body: Container(
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [

                Builder(
                  builder:(context){
                    return GestureDetector(child: logo(context),
                      onTap:
                          (){
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context)=> new Login() )); },
                  );}
                ),
                Center(child: Text('PeFUM',style: TextStyle(fontWeight: FontWeight.w700, fontSize: 21), )),
              ],
            )

          )
        ),
      )

    );
  }
}
